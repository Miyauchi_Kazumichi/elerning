package jp.co.elearning.mapper;

import java.util.List;

import jp.co.elearning.entity.Choices4ent;
import jp.co.elearning.entity.Question;

public interface QuestionMapper {
    List<Question> getQuestion();
    List<Choices4ent> getChoicesAll();
    List<Choices4ent> getCorrectChoices();
    // int getUserProogress(int id);
}
