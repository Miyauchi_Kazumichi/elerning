package jp.co.elearning.mapper;

import java.util.List;

import jp.co.elearning.entity.Answer;
import jp.co.elearning.entity.Question;
import jp.co.elearning.entity.User;
import jp.co.elearning.form.LoginForm;
import jp.co.elearning.form.QuestionForm;
import jp.co.elearning.form.SettingForm;
import jp.co.elearning.form.UserForm;

public interface Mapper {

	 User getUser(LoginForm form);
	 int updateUserProgress(QuestionForm form);
	 List<User> getAllUsers();
	 int insertUser(UserForm form);
	 List<Question> getQuestionAll();
	 List<Answer> getAnswerAll();
	 int updatePassword(SettingForm form);
	 int updateDivisionId(SettingForm form);
	 User selectUser(Integer id);
	 User selectOldUser(UserForm form);
	 User selectUserProgress(Integer userId);

}
