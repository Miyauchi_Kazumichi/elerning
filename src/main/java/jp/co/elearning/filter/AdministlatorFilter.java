package jp.co.elearning.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import jp.co.elearning.dto.UserDto;

@WebFilter(urlPatterns ={"/management",  "/signup"})
public class AdministlatorFilter implements Filter{

	@Autowired
	HttpSession session;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException,ServletException{

		session = ((HttpServletRequest) request).getSession();
		UserDto user = (UserDto) session.getAttribute("loginUser");

		//List<String> errorMessages = new ArrayList<String>();

		if(user != null && user.getDivisionId() != 1){
			// セッションがNUllならば、ログイン画面へ飛ばす
			//errorMessages.add("管理者権限がありません");
			//session.setAttribute("errorMessages", errorMessages);
			((HttpServletResponse) response).sendRedirect("home");
			return;
		}

		chain.doFilter(request, response);

	}

	@Override
	public void init(FilterConfig config) throws ServletException{}

	@Override
	public void destroy(){}
}

