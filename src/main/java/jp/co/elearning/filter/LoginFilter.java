package jp.co.elearning.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;

import jp.co.elearning.dto.UserDto;



@WebFilter("/*")
public class LoginFilter implements Filter{

	@Autowired
	HttpSession session;

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException,ServletException{

		session = ((HttpServletRequest) request).getSession();
		//session.removeAttribute("errorMessages");
		UserDto user = (UserDto) session.getAttribute("loginUser");
		String requestPath = ((HttpServletRequest)request).getServletPath();
		//List<String> errorMessages = new ArrayList<String>();

		if(user == null && !requestPath.equals("/login")/* && !requestPath.contains("/css")*/){
			/* セッションがNUllならば、ログイン画面へ飛ばす
			errorMessages.add("ログインしてください");
			session.setAttribute("errorMessages", errorMessages);*/
			((HttpServletResponse) response).sendRedirect("login");
			return;
		}

		chain.doFilter(request, response);

	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO 自動生成されたメソッド・スタブ

	}

	@Override
	public void destroy() {
		// TODO 自動生成されたメソッド・スタブ

	}
}