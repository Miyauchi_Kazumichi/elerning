package jp.co.elearning.dto;

public class UserIdDto {

	private String id;

	public String getId() {
	     return id;
	}
	public void setId(String id) {
	     this.id = id;
	}
}
