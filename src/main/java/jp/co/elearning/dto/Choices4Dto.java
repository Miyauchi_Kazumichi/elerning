package jp.co.elearning.dto;

public class Choices4Dto {
	private String choiceText;

    public String getChoiceText() {
    	return choiceText;
    }
    public void setChoiceText(String choiceText) {
    	this.choiceText = choiceText;
    }
}