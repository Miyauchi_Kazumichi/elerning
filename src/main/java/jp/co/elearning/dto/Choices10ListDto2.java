package jp.co.elearning.dto;

import java.util.List;

public class Choices10ListDto2 {
    private List<String> choices4List;

    public List<String> getChoices4List() {
    	return choices4List;
    }
    public void setChoices4List(List<String> choices4List) {
    	this.choices4List = choices4List;
    }
}