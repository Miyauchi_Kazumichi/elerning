package jp.co.elearning.dto;

import java.util.List;

public class Choices10ListDto {
    private List<Choices4Dto> choices4List;

    public List<Choices4Dto> getChoices4List() {
    	return choices4List;
    }
    public void setChoices4List(List<Choices4Dto> choices4List) {
    	this.choices4List = choices4List;
    }
}
