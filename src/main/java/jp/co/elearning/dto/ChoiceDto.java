package jp.co.elearning.dto;

public class ChoiceDto {

	private Integer questionId;
	private String choiceText;
	private Integer isCorrect;

	public int getquestionId() {
		return questionId;
	}
	public void setQuestionId(int questionId) {
	    this.questionId = questionId;
	}
	public String getChoiceText() {
	     return choiceText;
	}
	public void setChoiceText(String choiceText) {
	     this.choiceText = choiceText;
	}
	public int getIsCorrect() {
		return isCorrect;
	}
	public void setIsCorrect(int isCorrect) {
	    this.isCorrect = isCorrect;
	}
}
