package jp.co.elearning.dto;

import java.util.Date;

public class UserDto {

	private Integer id;
	private String name;
	private String password;
	private String account;
	private Integer divisionId;
	private Date createdDate;
	private Date updatedDate;
	private Integer progress;
	//private String id2;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
	    this.id = id;
	}
	public String getName() {
	     return name;
	}
	public void setName(String name) {
	     this.name = name;
	}
	public String getPassword() {
	     return password;
	}
	public void setPassword(String password) {
	     this.password = password;
	}
	public String getAccount() {
	     return account;
	}
	public void setAccount(String account) {
	     this.account = account;
	}
	public Integer getDivisionId() {
	     return divisionId;
	}
	public void setDivisionId(Integer divisionId) {
	     this.divisionId = divisionId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getupdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	public Integer getProgress() {
	     return progress;
	}
	public void setProgress(Integer progress) {
	     this.progress = progress;
	}

	/*public String getId2() {
	     return id2;
	}
	public void setId2(String id2) {
	     this.id2 = id2;
	}*/

}
