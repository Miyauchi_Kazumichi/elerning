package jp.co.elearning.form;

public class QuestionForm {
    private String choiceText1;
    private String choiceText2;
    private String choiceText3;
    private String choiceText4;
    private String choiceText5;
    private String choiceText6;
    private String choiceText7;
    private String choiceText8;
    private String choiceText9;
    private String choiceText10;
    private int userProgress;
    private Integer id;

    public String getChoiceText1() {
    	return choiceText1;
    }
    public void setChoiceText1(String choiceText1) {
    	this.choiceText1 = choiceText1;
    }

    public String getChoiceText2() {
    	return choiceText2;
    }
    public void setChoiceText2(String choiceText2) {
    	this.choiceText2 = choiceText2;
    }

    public String getChoiceText3() {
    	return choiceText3;
    }
    public void setChoiceText3(String choiceText3) {
    	this.choiceText3 = choiceText3;
    }

    public String getChoiceText4() {
    	return choiceText4;
    }
    public void setChoiceText4(String choiceText4) {
    	this.choiceText4 = choiceText4;
    }

    public String getChoiceText5() {
    	return choiceText5;
    }
    public void setChoiceText5(String choiceText5) {
    	this.choiceText5 = choiceText5;
    }

    public String getChoiceText6() {
    	return choiceText6;
    }
    public void setChoiceText6(String choiceText6) {
    	this.choiceText6 = choiceText6;
    }

    public String getChoiceText7() {
    	return choiceText7;
    }
    public void setChoiceText7(String choiceText7) {
    	this.choiceText7 = choiceText7;
    }

    public String getChoiceText8() {
    	return choiceText8;
    }
    public void setChoiceText8(String choiceText8) {
    	this.choiceText8 = choiceText8;
    }

    public String getChoiceText9() {
    	return choiceText9;
    }
    public void setChoiceText9(String choiceText9) {
    	this.choiceText9 = choiceText9;
    }

    public String getChoiceText10() {
    	return choiceText10;
    }
    public void setChoiceText10(String choiceText10) {
    	this.choiceText10 = choiceText10;
    }

    public int getUserProgress() {
    	return userProgress;
    }
    public void setUserProgress(int userProgress) {
    	this.userProgress = userProgress;
    }

    public Integer getId() {
    	return id;
    }
    public void setId(Integer id) {
    	this.id = id;
    }
}
