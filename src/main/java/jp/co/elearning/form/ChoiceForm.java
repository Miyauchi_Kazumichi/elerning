package jp.co.elearning.form;

public class ChoiceForm {

	private Integer id;
	private Integer questionId;
	private String choiceText;
	private Integer isCorrect;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
	    this.id = id;
	}

	public Integer getquestionId() {
		return questionId;
	}
	public void setQuestionId(Integer questionId) {
	    this.questionId = questionId;
	}
	public String getChoiceText() {
	     return choiceText;
	}
	public void setChoiceText(String choiceText) {
	     this.choiceText = choiceText;
	}
	public Integer getIsCorrect() {
		return isCorrect;
	}
	public void setIsCorrect(Integer isCorrect) {
	    this.isCorrect = isCorrect;
	}
}
