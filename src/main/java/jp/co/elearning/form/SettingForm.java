package jp.co.elearning.form;

import javax.validation.constraints.Pattern;

public class SettingForm {
	private Integer id;
	private int divisionId;

	@Pattern(message = "パスワードは記号を含む、全ての半角英数字で、6文字から20文字で入力してください", regexp = "^[-_@+*;:#$%&A-Za-z0-9]{8,20}$")
	private String password;

	private String passwordConfirm;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public int getDivisionId() {
		return divisionId;
	}
	public void setDivisionId(int divisionId) {
		this.divisionId = divisionId;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getPasswordConfirm() {
	     return passwordConfirm;
	}
	public void setPasswordConfirm(String passwordConfirm) {
	     this.passwordConfirm = passwordConfirm;
	}

}
