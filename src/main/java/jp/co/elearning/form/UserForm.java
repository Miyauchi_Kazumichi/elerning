package jp.co.elearning.form;

import java.util.Date;

import javax.validation.constraints.Pattern;


public class UserForm {

	private Integer id;

	@Pattern(message = "氏名は1文字から10文字で入力してください", regexp = "^[ぁ-んァ-ン一-龥A-Za-z]{2,10}$")
	private String name;

	@Pattern(message = "パスワードは記号を含む、全ての半角英数字で、6文字から20文字で入力してください", regexp = "^[-_@+*;:#$%&A-Za-z0-9]{6,20}$")
	private String password;

	@Pattern(message = "アカウント名は半角英数字で、6文字から20文字で入力してください", regexp = "^[A-Za-z0-9]{1,10}$")
	private String account;

	private String passwordConfirm;
	private Integer divisionId;
	private Date created_date;
	private Date updated_date;
	private Integer progress;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
	    this.id = id;
	}
	public String getName() {
	     return name;
	}
	public void setName(String name) {
	     this.name = name;
	}
	public String getPassword() {
	     return password;
	}
	public void setPassword(String password) {
	     this.password = password;
	}
	public String getPasswordConfirm() {
	     return passwordConfirm;
	}
	public void setPasswordConfirm(String passwordConfirm) {
	     this.passwordConfirm = passwordConfirm;
	}
	public String getAccount() {
	     return account;
	}
	public void setAccount(String account) {
	     this.account = account;
	}
	public Integer getDivisionId() {
	     return divisionId;
	}
	public void setDivisionId(Integer divisionId) {
	     this.divisionId = divisionId;
	}

	public Date getCreatedDate() {
		return created_date;
	}

	public void setCreatedDate(Date created_date) {
		this.created_date = created_date;
	}

	public Date getupdatedDate() {
		return updated_date;
	}

	public void setUpdatedDate(Date updated_date) {
		this.updated_date = updated_date;
	}
	public Integer getProgress() {
	     return progress;
	}
	public void setProgress(Integer progress) {
	     this.progress = progress;
	}

}
