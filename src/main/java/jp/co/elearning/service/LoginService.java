package jp.co.elearning.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import jp.co.elearning.dto.UserDto;
import jp.co.elearning.entity.User;
import jp.co.elearning.form.LoginForm;
import jp.co.elearning.mapper.Mapper;
import jp.co.elearning.utils.CipherUtil;
@Service
public class LoginService {

	@Autowired
    private Mapper mapper;

	public UserDto getUser(LoginForm form) {
		if(!StringUtils.isEmpty(form.getPassword())) {
			String encPassword = CipherUtil.encrypt(form.getPassword());
			form.setPassword(encPassword);
		}
		User entity = mapper.getUser(form);
		if (entity == null) {
			return null;
		}
		UserDto dto = new UserDto();
		BeanUtils.copyProperties(entity, dto);

		return dto;
	}

	public List<UserDto> getAllUsers(){
		List<User> userList = mapper.getAllUsers();
		List<UserDto> resultList = convertToDto(userList);
		return resultList;
	}

	private List<UserDto> convertToDto(List<User> userList) {
		List<UserDto> resultList = new LinkedList<UserDto>();
		for (User entity : userList) {
			UserDto dto = new UserDto();
			BeanUtils.copyProperties(entity, dto);
			resultList.add(dto);
		}
		return resultList;
	}

	public UserDto selectUserProgress(Integer userId) {
		User entity = mapper.selectUserProgress(userId);
		UserDto dto = new UserDto();
		BeanUtils.copyProperties(entity, dto);
		return dto;
	}
}