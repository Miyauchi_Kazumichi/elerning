package jp.co.elearning.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import jp.co.elearning.dto.UserDto;
import jp.co.elearning.entity.User;
import jp.co.elearning.form.SettingForm;
import jp.co.elearning.mapper.Mapper;
import jp.co.elearning.utils.CipherUtil;

@Service
public class SettingService {
	@Autowired
	private Mapper mapper;

	public UserDto selectUser(Integer id) {
		User entity = mapper.selectUser(id);
		UserDto dto = new UserDto();
		if (entity == null) {
			return null;
		}
		BeanUtils.copyProperties(entity, dto);
		return dto;

	}
	public int updateDivisionId(SettingForm form) {

		/*if(!StringUtils.isEmpty(form.getPassword())) {
			String encPassword = CipherUtil.encrypt(form.getPassword());
			form.setPassword(encPassword);
		}*/
		int updateDivisionId = mapper.updateDivisionId(form);
		return updateDivisionId;
	}

	public int updatePassword(SettingForm form) {

		if(!StringUtils.isEmpty(form.getPassword())) {
			String encPassword = CipherUtil.encrypt(form.getPassword());
			form.setPassword(encPassword);
		}

		int updatePassword = mapper.updatePassword(form);
		return updatePassword;
	}
}
