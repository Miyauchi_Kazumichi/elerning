package jp.co.elearning.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.elearning.dto.Choices4Dto;
import jp.co.elearning.dto.QuestionDto;
import jp.co.elearning.entity.Choices4ent;
import jp.co.elearning.entity.Question;
import jp.co.elearning.mapper.QuestionMapper;

@Service
public class QuestionService {

	@Autowired
	private QuestionMapper questionMapper;

	public List<QuestionDto> getQuestion() {
        List<Question> questionList = questionMapper.getQuestion();
        List<QuestionDto> resultList = convertToQuestionDto(questionList);
        return resultList;
    }

    private List<QuestionDto> convertToQuestionDto(List<Question> questionList) {
        List<QuestionDto> resultList = new LinkedList<QuestionDto>();
        for (Question entity : questionList) {
            QuestionDto dto = new QuestionDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
    }

    public List<Choices4Dto> getChoicesAll2() {
        List<Choices4ent> choicesAll = questionMapper.getChoicesAll();
        List<Choices4Dto> resultList = convertToChoices4Dto2(choicesAll);

    	return resultList;
    }

    private List<Choices4Dto> convertToChoices4Dto2(List<Choices4ent> list) {
        List<Choices4Dto> resultList = new LinkedList<Choices4Dto>();
        for(Choices4ent entity: list) {
        	Choices4Dto dto = new Choices4Dto();
        	BeanUtils.copyProperties(entity, dto);
        	resultList.add(dto);
        }

    	return resultList;
    }

    public List<Choices4Dto> getCorrectChoices() {
        List<Choices4ent> choicesAll = questionMapper.getCorrectChoices();
        List<Choices4Dto> resultList = convertToCorrectChoices(choicesAll);

    	return resultList;
    }

    private List<Choices4Dto> convertToCorrectChoices(List<Choices4ent> list) {
        List<Choices4Dto> resultList = new LinkedList<Choices4Dto>();
        for(Choices4ent entity: list) {
        	Choices4Dto dto = new Choices4Dto();
        	BeanUtils.copyProperties(entity, dto);
        	resultList.add(dto);
        }
    	return resultList;
    }
/*
    public int getUserProgress(int id) {
    	int progress = questionMapper.getUserProogress(id);
    	return progress;
    }
*/


}
