package jp.co.elearning.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import jp.co.elearning.dto.UserDto;
import jp.co.elearning.entity.User;
import jp.co.elearning.form.UserForm;
import jp.co.elearning.mapper.Mapper;
import jp.co.elearning.utils.CipherUtil;

@Service
public class SignUpService {

	@Autowired
	private Mapper mapper;

	public int insertUser(UserForm form) {

		if(!StringUtils.isEmpty(form.getPassword())) {
			String encPassword = CipherUtil.encrypt(form.getPassword());
			form.setPassword(encPassword);
		}

		int count = mapper.insertUser(form);
		return count;
	}

	public UserDto selectOldUser(UserForm form) {
		User entity = mapper.selectOldUser(form);
			if (entity == null) {
				return null;
			}
			UserDto dto = new UserDto();
			BeanUtils.copyProperties(entity, dto);

			return dto;
		}
}