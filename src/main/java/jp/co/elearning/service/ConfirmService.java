package jp.co.elearning.service;

import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.elearning.dto.AnswerDto;
import jp.co.elearning.dto.QuestionDto;
import jp.co.elearning.entity.Answer;
import jp.co.elearning.entity.Question;
import jp.co.elearning.form.QuestionForm;
import jp.co.elearning.mapper.Mapper;

@Service
public class ConfirmService {

	@Autowired Mapper mapper;

	public List<QuestionDto> getQuestionAll() {
		List<Question> questionList = mapper.getQuestionAll();
		List<QuestionDto> resultQuestionList = convertQuestionToDto(questionList);
		return resultQuestionList;
	}

	public List<QuestionDto> convertQuestionToDto(List<Question> questionList) {
		List<QuestionDto> resultQuestionList = new LinkedList<QuestionDto>();
		for (Question entity : questionList) {
			QuestionDto dto = new QuestionDto();
			BeanUtils.copyProperties(entity, dto);
			resultQuestionList.add(dto);
		}
		return resultQuestionList;
	}

	public List<AnswerDto> getAnswerAll() {
		List<Answer> answerList = mapper.getAnswerAll();
		List<AnswerDto> resultAnswerList = convertAnswerToDto(answerList);
		return resultAnswerList;
	}

	public List<AnswerDto> convertAnswerToDto(List<Answer> answerList) {
		List<AnswerDto> resultAnswerList = new LinkedList<AnswerDto>();
		for (Answer entity : answerList) {
			AnswerDto dto = new AnswerDto();
			BeanUtils.copyProperties(entity, dto);
			resultAnswerList.add(dto);
		}
		return resultAnswerList;
	}

	public int updateUserProgress(QuestionForm form) {
		int count = mapper.updateUserProgress(form);
		return count;
	}

}
