package jp.co.elearning.entity;

public class Answer {

	private Integer id;
	private String explanation;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
	    this.id = id;
	}

	public String getExplanation() {
	     return explanation;
	}
	public void setExplanation(String explanation) {
	     this.explanation = explanation;
	}
}
