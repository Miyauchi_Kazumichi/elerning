package jp.co.elearning.entity;

import java.util.Date;

public class User {

	private Integer id;
	private String name;
	private String password;
	private String account;
	private Integer divisionId;
	private Date created_date;
	private Date updated_date;
	private Integer progress;

	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
	    this.id = id;
	}
	public String getName() {
	     return name;
	}
	public void setName(String name) {
	     this.name = name;
	}
	public String getPassword() {
	     return password;
	}
	public void setPassword(String password) {
	     this.password = password;
	}
	public String getAccount() {
	     return account;
	}
	public void setAccount(String account) {
	     this.account = account;
	}
	public Integer getDivisionId() {
	     return divisionId;
	}
	public void setDivisionId(Integer divisionId) {
	     this.divisionId = divisionId;
	}

	public Date getCreatedDate() {
		return created_date;
	}

	public void setCreatedDate(Date created_date) {
		this.created_date = created_date;
	}

	public Date getupdatedDate() {
		return updated_date;
	}

	public void setUpdatedDate(Date updated_date) {
		this.updated_date = updated_date;
	}
	public Integer getProgress() {
	     return progress;
	}
	public void setProgress(Integer progress) {
	     this.progress = progress;
	}

}
