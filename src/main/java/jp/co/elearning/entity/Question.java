package jp.co.elearning.entity;

public class Question{
    private Integer id;
    private String question;
    private Integer answer_id;

    public Integer getId() {
    	return id;
    }
    public void setId(Integer id) {
    	this.id = id;
    }

    public String getQuestion() {
    	return question;
    }
    public void setQuestion(String question) {
    	this.question = question;
    }

    public Integer getAnswerId() {
    	return answer_id;
    }
    public void setAnswerId(Integer answerId) {
    	this.answer_id = answerId;
    }

}
