package jp.co.elearning.controller;

import javax.validation.Valid;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.elearning.dto.UserDto;
import jp.co.elearning.form.UserForm;
import jp.co.elearning.service.SignUpService;

@Controller
public class SignUpController {

	@Autowired
	private SignUpService signUpService;

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String user(Model model) {

		//model.addAttribute("message", "新規登録");
		return "signup";
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String userInsert(@Valid @ModelAttribute UserForm form, BindingResult result, Model model) {

		if(result.hasErrors()) {
			model.addAttribute("message", "新規登録");
			model.addAttribute("account", form.getAccount());
			model.addAttribute("name", form.getName());
			return "signup";
		}

		UserDto user = signUpService.selectOldUser(form);

		if(user != null) {
			model.addAttribute("account", form.getAccount());
			model.addAttribute("name", form.getName());
			model.addAttribute("errorMessages", "アカウントが重複しています");
			return "signup";
		}

		if(!form.getPassword().equals(form.getPasswordConfirm())) {
			model.addAttribute("account", form.getAccount());
			model.addAttribute("name", form.getName());
			model.addAttribute("message", "以下のエラーを解消してください");
			model.addAttribute("errorMessages", "パスワードと確認用パスワードが一致していません");
			return "signup";
		}
		int count = signUpService.insertUser(form);
		model.addAttribute("userForm", form);
		Logger.getLogger(SignUpController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
		return "redirect:/home";
	}
}
