package jp.co.elearning.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.elearning.form.UserForm;


@Controller
public class LogoutController {
	@Autowired
	HttpSession session;

	@Autowired
	HttpServletRequest request;

	@RequestMapping(value = "/logout/", method = RequestMethod.GET)
	public String logoutUser(Model model, UserForm form, int id) {
		return "home";
	}

	@RequestMapping(value = "/logout/", method = RequestMethod.POST)
	public String logoutUser() {
		// セッションの無効化
	    session.invalidate();

	    session = request.getSession(true);
	    return "redirect:/login";
	}
}
