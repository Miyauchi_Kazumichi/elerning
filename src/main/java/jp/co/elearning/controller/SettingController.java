package jp.co.elearning.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.elearning.dto.UserDto;
import jp.co.elearning.dto.UserIdDto;
import jp.co.elearning.entity.User;
import jp.co.elearning.form.SettingForm;
import jp.co.elearning.service.LoginService;
import jp.co.elearning.service.SettingService;

@Controller
public class SettingController {

	@Autowired
	private SettingService settingService;

	@Autowired
	private LoginService loginService;

	@Autowired
	HttpSession session;

	@Autowired
	HttpServletRequest request;

	@RequestMapping(value = "/setting/{id}", method = RequestMethod.GET)
	public String selectUser(Model model, @PathVariable ("id") String id) {

		UserDto userDto = (UserDto) session.getAttribute("loginUser");
		Integer userId = userDto.getId();
		UserDto user = loginService.selectUserProgress(userId);
		model.addAttribute("user", user);
		UserIdDto dto = new UserIdDto();
		dto.setId(id);

		if (!id.matches("^[0-9]+$") || id.isEmpty()) {
			model.addAttribute("errorMessages", "不正なパラメーターが入力されました");
			return "home";
		}else {
			UserDto checkUser = settingService.selectUser(Integer.parseInt(dto.getId()));
			model.addAttribute("checkUser", checkUser);
			if(checkUser == null) {
				model.addAttribute("errorMessages", "不正なパラメーターが入力されました");
				return "home";
			}
		}
		return "setting";
	}

	@RequestMapping(value = "/setting/{id}", method = RequestMethod.POST)
	public String userUpdate(@Valid @ModelAttribute SettingForm form, BindingResult result, User user, Model model) {

		if(StringUtils.isEmpty(form.getPassword()) && form.getDivisionId() == 0) {
			session.setAttribute("validation", "パスワードは記号を含む、全ての半角英数字で、8文字から20文字で入力してください");
			return "redirect:/setting/{id}";
		}

		if(result.hasErrors()) {
			session.setAttribute("validation", "パスワードは記号を含む、全ての半角英数字で、8文字から20文字で入力してください");
			session.setAttribute("errorMessages", "パスワードと確認用パスワードが一致していません");
			model.addAttribute("settingForm", form);
			return "redirect:/setting/{id}";
		}

		if(StringUtils.isEmpty(form.getPassword())) {
			settingService.updateDivisionId(form);
			model.addAttribute("settingForm", form);
			return "redirect:/home";
		}else if(form.getDivisionId() == 0) {
			if(result.hasErrors()) {
				session.setAttribute("validation", "パスワードは記号を含む、全ての半角英数字で、8文字から20文字で入力してください");
				session.setAttribute("errorMessages", "パスワードと確認用パスワードが一致していません");
				model.addAttribute("settingForm", form);
				return "redirect:/setting/{id}";
			} else {
				if(!form.getPassword().equals(form.getPasswordConfirm())) {
					session.setAttribute("errorMessages", "パスワードと確認用パスワードが一致していません");
					return "redirect:/setting/{id}";
				}
			settingService.updatePassword(form);
			model.addAttribute("settingForm", form);

			}
		}
		return "redirect:/home";
	}
}
