package jp.co.elearning.controller;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.elearning.dto.UserDto;
import jp.co.elearning.form.LoginForm;
import jp.co.elearning.service.LoginService;

@Controller
public class LoginController {
	@Autowired
	HttpSession session;
	@Autowired
	private LoginService loginService;

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String showMessage(Model model) {
		model.addAttribute("message", "ログインしてください");
		return "login";
	}

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(@Valid @ModelAttribute LoginForm form, BindingResult result, Model model) {
		String password = form.getPassword();
		UserDto loginUser = loginService.getUser(form);
		String account = form.getAccount();

		if (loginUser == null) {
			model.addAttribute("account",account);
			model.addAttribute("message", "ログインに失敗しました");
			return "login";

		}else if(password.matches("[0-9]{7}")) {

			String str = "redirect:/setting/" + loginUser.getId();
			session.setAttribute("passwordMessage", "パスワードを変更してください");
			session.setAttribute("loginUser", loginUser);
			return str;
		}

		session.setAttribute("loginUser", loginUser);
		return "redirect:/home";
	}
}
