package jp.co.elearning.controller;

import java.util.LinkedList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.elearning.dto.AnswerDto;
import jp.co.elearning.dto.Choices10ListDto2;
import jp.co.elearning.dto.Choices4Dto;
import jp.co.elearning.dto.QuestionDto;
import jp.co.elearning.dto.UserDto;
import jp.co.elearning.form.QuestionForm;
import jp.co.elearning.form.UserForm;
import jp.co.elearning.service.ConfirmService;
import jp.co.elearning.service.LoginService;
import jp.co.elearning.service.QuestionService;

@Controller
public class QuestionController {

	@Autowired
	private QuestionService questionService;

	@Autowired
	HttpSession session;

	@Autowired
	HttpServletRequest request;

	@Autowired
	private ConfirmService confirmService;

	@Autowired
	private LoginService loginService;

	@RequestMapping(value = "/question1", method = RequestMethod.GET)
	public String disp(Model model) {
		List<Choices10ListDto2> choices10List2 = getChoices10List2();
		QuestionForm form = new QuestionForm();
		List<QuestionDto> questionList = questionService.getQuestion();

	    model.addAttribute("question1", questionList.get(0).getQuestion());
	    model.addAttribute("question2", questionList.get(1).getQuestion());
	    model.addAttribute("question3", questionList.get(2).getQuestion());
	    model.addAttribute("question4", questionList.get(3).getQuestion());
	    model.addAttribute("question5", questionList.get(4).getQuestion());
	    model.addAttribute("question6", questionList.get(5).getQuestion());
	    model.addAttribute("question7", questionList.get(6).getQuestion());
	    model.addAttribute("question8", questionList.get(7).getQuestion());
	    model.addAttribute("question9", questionList.get(8).getQuestion());
	    model.addAttribute("question10", questionList.get(9).getQuestion());

		List<String> choices40List = choices10List2.get(0).getChoices4List();
		form.setChoiceText1(choices40List.get(0));
	    model.addAttribute("choices40", choices40List);
	    List<String> choices41List = choices10List2.get(1).getChoices4List();
	    form.setChoiceText2(choices41List.get(0));
	    model.addAttribute("choices41", choices41List);
	    List<String> choices42List = choices10List2.get(2).getChoices4List();
	    form.setChoiceText3(choices42List.get(0));
	    model.addAttribute("choices42", choices42List);
	    List<String> choices43List = choices10List2.get(3).getChoices4List();
	    form.setChoiceText4(choices43List.get(0));
	    model.addAttribute("choices43", choices43List);
	    List<String> choices44List = choices10List2.get(4).getChoices4List();
	    form.setChoiceText5(choices44List.get(0));
	    model.addAttribute("choices44", choices44List);
	    List<String> choices45List = choices10List2.get(5).getChoices4List();
	    form.setChoiceText6(choices45List.get(0));
	    model.addAttribute("choices45", choices45List);
	    List<String> choices46List = choices10List2.get(6).getChoices4List();
	    form.setChoiceText7(choices46List.get(0));
	    model.addAttribute("choices46", choices46List);
	    List<String> choices47List = choices10List2.get(7).getChoices4List();
	    form.setChoiceText8(choices47List.get(0));
	    model.addAttribute("choices47", choices47List);
	    List<String> choices48List = choices10List2.get(8).getChoices4List();
	    form.setChoiceText9(choices48List.get(0));
	    model.addAttribute("choices48", choices48List);
	    List<String> choices49List = choices10List2.get(9).getChoices4List();
	    form.setChoiceText10(choices49List.get(0));
	    model.addAttribute("choices49", choices49List);

	    model.addAttribute("questionForm", form);
	    return "question1";
	}

	@RequestMapping(value = "/confirm", method = RequestMethod.GET)
	public String disp(@ModelAttribute QuestionForm form,  Model model) {
		List<Choices4Dto> correctChoices =  questionService.getCorrectChoices();
		List<QuestionDto> questionList = questionService.getQuestion();
		int count = 0;
		if(correctChoices.get(0).getChoiceText().equals(form.getChoiceText1())) {
			count += 1;
		}
		if(correctChoices.get(1).getChoiceText().equals(form.getChoiceText2())) {
			count += 1;
		}
		if(correctChoices.get(2).getChoiceText().equals(form.getChoiceText3())) {
			count += 1;
		}
		if(correctChoices.get(3).getChoiceText().equals(form.getChoiceText4())) {
			count += 1;
		}
		if(correctChoices.get(4).getChoiceText().equals(form.getChoiceText5())) {
			count += 1;
		}
		if(correctChoices.get(5).getChoiceText().equals(form.getChoiceText6())) {
			count += 1;
		}
		if(correctChoices.get(6).getChoiceText().equals(form.getChoiceText7())) {
			count += 1;
		}
		if(correctChoices.get(7).getChoiceText().equals(form.getChoiceText8())) {
			count += 1;
		}
		if(correctChoices.get(8).getChoiceText().equals(form.getChoiceText9())) {
			count += 1;
		}
		if(correctChoices.get(9).getChoiceText().equals(form.getChoiceText10())) {
			count += 1;
		}
		if(count == 10) {
			form.setUserProgress(2);
		} else {
			form.setUserProgress(1);
		}

		session.setAttribute("choice1", form.getChoiceText1());
		session.setAttribute("choice2", form.getChoiceText2());
		session.setAttribute("choice3", form.getChoiceText3());
		session.setAttribute("choice4", form.getChoiceText4());
		session.setAttribute("choice5", form.getChoiceText5());
		session.setAttribute("choice6", form.getChoiceText6());
		session.setAttribute("choice7", form.getChoiceText7());
		session.setAttribute("choice8", form.getChoiceText8());
		session.setAttribute("choice9", form.getChoiceText9());
		session.setAttribute("choice10", form.getChoiceText10());
/*
		model.addAttribute("choice1", form.getChoiceText1());
		model.addAttribute("choice2", form.getChoiceText2());
		model.addAttribute("choice3", form.getChoiceText3());
		model.addAttribute("choice4", form.getChoiceText4());
		model.addAttribute("choice5", form.getChoiceText5());
		model.addAttribute("choice6", form.getChoiceText6());
		model.addAttribute("choice7", form.getChoiceText7());
		model.addAttribute("choice8", form.getChoiceText8());
		model.addAttribute("choice9", form.getChoiceText9());
		model.addAttribute("choice10", form.getChoiceText10());
*/


		model.addAttribute("userProgress", form.getUserProgress());

		model.addAttribute("questionForm", form);

	    model.addAttribute("question1", questionList.get(0).getQuestion());
	    model.addAttribute("question2", questionList.get(1).getQuestion());
	    model.addAttribute("question3", questionList.get(2).getQuestion());
	    model.addAttribute("question4", questionList.get(3).getQuestion());
	    model.addAttribute("question5", questionList.get(4).getQuestion());
	    model.addAttribute("question6", questionList.get(5).getQuestion());
	    model.addAttribute("question7", questionList.get(6).getQuestion());
	    model.addAttribute("question8", questionList.get(7).getQuestion());
	    model.addAttribute("question9", questionList.get(8).getQuestion());
	    model.addAttribute("question10", questionList.get(9).getQuestion());

		return "confirm";
	}

	public List<Choices10ListDto2> getChoices10List2() {
		List<Choices10ListDto2> resultList10 = new LinkedList<Choices10ListDto2>();
		List<Choices4Dto> choicesAll = questionService.getChoicesAll2();
		int sizeLength = choicesAll.size() / 4;
		int sizeChoice = choicesAll.size() /sizeLength;
		int i = 0;
		int j = 0;
		for(i = 0; i < sizeLength; i++) {
			List<String> resultList4 = new LinkedList<String>();
			Choices10ListDto2 choices10 = new Choices10ListDto2();
			for(j = 0; j < sizeChoice; j++) {
				String choices4 = choicesAll.get(i * 4 + j).getChoiceText();
				resultList4.add(choices4);
			}
			j = 0;
			choices10.setChoices4List(resultList4);
			resultList10.add(choices10);
		}
		return resultList10;
	}

	@RequestMapping(value = "/result", method = RequestMethod.GET)
	public String result(@ModelAttribute QuestionForm form, Model model) {
		List<QuestionDto> questionList = questionService.getQuestion();
		List<Choices4Dto> correctChoices =  questionService.getCorrectChoices();
		List<Choices10ListDto2> choices10List2 = getChoices10List2();

	    List<AnswerDto> explanations = confirmService.getAnswerAll();
		model.addAttribute("questionForm", form);

		String message;
		UserDto user = (UserDto) session.getAttribute("loginUser");
		form.setId(user.getId());

        if(user.getProgress() != 2) {
        	int count = confirmService.updateUserProgress(form);
			Logger.getLogger(QuestionController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
        }
        if(form.getUserProgress() == 2) {
		    message = "おめでとう！合格です！";
        } else {
        	message = "残念、不合格です…";
        }
	    model.addAttribute("questionForm", form);
    	model.addAttribute("message", message);

	    model.addAttribute("correctAnswer1", correctChoices.get(0).getChoiceText());
	    model.addAttribute("correctAnswer2", correctChoices.get(1).getChoiceText());
	    model.addAttribute("correctAnswer3", correctChoices.get(2).getChoiceText());
	    model.addAttribute("correctAnswer4", correctChoices.get(3).getChoiceText());
	    model.addAttribute("correctAnswer5", correctChoices.get(4).getChoiceText());
	    model.addAttribute("correctAnswer6", correctChoices.get(5).getChoiceText());
	    model.addAttribute("correctAnswer7", correctChoices.get(6).getChoiceText());
	    model.addAttribute("correctAnswer8", correctChoices.get(7).getChoiceText());
	    model.addAttribute("correctAnswer9", correctChoices.get(8).getChoiceText());
	    model.addAttribute("correctAnswer10", correctChoices.get(9).getChoiceText());

	    model.addAttribute("question1", questionList.get(0).getQuestion());
	    model.addAttribute("question2", questionList.get(1).getQuestion());
	    model.addAttribute("question3", questionList.get(2).getQuestion());
	    model.addAttribute("question4", questionList.get(3).getQuestion());
	    model.addAttribute("question5", questionList.get(4).getQuestion());
	    model.addAttribute("question6", questionList.get(5).getQuestion());
	    model.addAttribute("question7", questionList.get(6).getQuestion());
	    model.addAttribute("question8", questionList.get(7).getQuestion());
	    model.addAttribute("question9", questionList.get(8).getQuestion());
	    model.addAttribute("question10", questionList.get(9).getQuestion());

	    model.addAttribute("answer1", explanations.get(0).getExplanation());
	    model.addAttribute("answer2", explanations.get(1).getExplanation());
	    model.addAttribute("answer3", explanations.get(2).getExplanation());
	    model.addAttribute("answer4", explanations.get(3).getExplanation());
	    model.addAttribute("answer5", explanations.get(4).getExplanation());
	    model.addAttribute("answer6", explanations.get(5).getExplanation());
	    model.addAttribute("answer7", explanations.get(6).getExplanation());
	    model.addAttribute("answer8", explanations.get(7).getExplanation());
	    model.addAttribute("answer9", explanations.get(8).getExplanation());
	    model.addAttribute("answer10", explanations.get(9).getExplanation());

	    List<String> choices40List = choices10List2.get(0).getChoices4List();
		form.setChoiceText1(choices40List.get(0));
	    model.addAttribute("choices40", choices40List);
	    List<String> choices41List = choices10List2.get(1).getChoices4List();
	    form.setChoiceText2(choices41List.get(0));
	    model.addAttribute("choices41", choices41List);
	    List<String> choices42List = choices10List2.get(2).getChoices4List();
	    form.setChoiceText3(choices42List.get(0));
	    model.addAttribute("choices42", choices42List);
	    List<String> choices43List = choices10List2.get(3).getChoices4List();
	    form.setChoiceText4(choices43List.get(0));
	    model.addAttribute("choices43", choices43List);
	    List<String> choices44List = choices10List2.get(4).getChoices4List();
	    form.setChoiceText5(choices44List.get(0));
	    model.addAttribute("choices44", choices44List);
	    List<String> choices45List = choices10List2.get(5).getChoices4List();
	    form.setChoiceText6(choices45List.get(0));
	    model.addAttribute("choices45", choices45List);
	    List<String> choices46List = choices10List2.get(6).getChoices4List();
	    form.setChoiceText7(choices46List.get(0));
	    model.addAttribute("choices46", choices46List);
	    List<String> choices47List = choices10List2.get(7).getChoices4List();
	    form.setChoiceText8(choices47List.get(0));
	    model.addAttribute("choices47", choices47List);
	    List<String> choices48List = choices10List2.get(8).getChoices4List();
	    form.setChoiceText9(choices48List.get(0));
	    model.addAttribute("choices48", choices48List);
	    List<String> choices49List = choices10List2.get(9).getChoices4List();
	    form.setChoiceText10(choices49List.get(0));
	    model.addAttribute("choices49", choices49List);
	    return "result";

	}

	@RequestMapping(value = "/result", method = RequestMethod.POST)
	public String resultSend(@ModelAttribute UserForm form, Model model) {

		/*UserDto user = (UserDto) session.getAttribute("loginUser");
        if(user.getProgress() != 2) {
		    int count = confirmService.updateUserProgress(form);
			Logger.getLogger(QuestionController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
        }*/
		model.addAttribute("userForm", form);
		session.setAttribute("newProgress", form);
		return "redirect:/home";
	}
}


