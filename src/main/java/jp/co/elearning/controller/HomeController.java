package jp.co.elearning.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.elearning.dto.UserDto;
import jp.co.elearning.form.UserForm;
import jp.co.elearning.service.LoginService;


@Controller
public class HomeController {


	/*@Autowired
	private LoginService loginService;*/

	@Autowired
	HttpSession session;
	@Autowired
	HttpServletRequest request;
	@Autowired
	private LoginService loginService;

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String loginUser(@ModelAttribute UserForm form, Model model) {
		UserDto dto = (UserDto) session.getAttribute("loginUser");
		Integer userId = dto.getId();
		UserDto user = loginService.selectUserProgress(userId);
		model.addAttribute("user", user);
		return "home";
	}

}
