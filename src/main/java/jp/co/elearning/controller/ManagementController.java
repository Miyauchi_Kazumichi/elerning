package jp.co.elearning.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.elearning.dto.UserDto;
import jp.co.elearning.form.SettingForm;
import jp.co.elearning.service.LoginService;

@Controller
public class ManagementController {
	@Autowired
	private LoginService loginService;

	@RequestMapping(value = "/management", method = RequestMethod.GET)
	public String allUsers(Model model) {
		List<UserDto> users = loginService.getAllUsers();
		model.addAttribute("users", users);
		return "management";
	}

	@RequestMapping(value = "/management", method = RequestMethod.POST)
	public String postManagement(@ModelAttribute SettingForm form, Model model) {
		model.addAttribute("settingForm", form);
		return "redirect:/setting/{id}";
	}
}
