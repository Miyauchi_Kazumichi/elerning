<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
<meta charset="UTF-8">
<title>ELearning</title>
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet" >

</head>
<body>
	<a href="/ELearningTeam/home"><img src="https://www.alhinc.jp/img/common/h-logo.svg" width="150" height="100"></a>
	<c:remove var = "answer1" scope = "session"/>
	<c:remove var = "answer2" scope = "session"/>
	<c:remove var = "answer3" scope = "session"/>
	<c:remove var = "answer4" scope = "session"/>
	<c:remove var = "answer5" scope = "session"/>
	<c:remove var = "answer6" scope = "session"/>
	<c:remove var = "answer7" scope = "session"/>
	<c:remove var = "answer8" scope = "session"/>
	<c:remove var = "answer9" scope = "session"/>
	<c:remove var = "answer10" scope = "session"/>
	<c:remove var = "errorMessages" scope = "session"/>
	<c:remove var = "checkUser" scope = "session"/>
	<c:remove var = "validation" scope = "session"/>

	<div class="main-contents">
		<h1>ようこそ${loginUser.name}さん</h1>
		<h1>ホーム画面</h1>
		<div class="header">
			<form:form action="{logout}" method="post">

				<a href="/ELearningTeam/login" class="button">ログアウト</a>
			</form:form>
			<form:form modelAttribute="userForm" action="/ELearningTeam/home">
				<input type = "hidden" value="${loginUser.id}" name = "id" id = "id">
			</form:form>
			<c:if test="${loginUser.divisionId==1}">
				<a href="/ELearningTeam/management/" class="button">ユーザー一覧</a>
			</c:if>
			<c:if test="${loginUser.divisionId==2}">
				<a href="/ELearningTeam/setting/${loginUser.id}" class="button">パスワード変更</a>
			</c:if>
			<br />
			<div class="homeprogress">
			<br/><br/>
			<c:out value = "${errorMessages}"></c:out>
			<br /><br/>
			受験状況:
			<c:if test="${user.progress==0}"><span class="progress">未受験</span></c:if>
			<c:if test="${user.progress==1}"><span class="progress">不合格</span></c:if>
			<c:if test="${user.progress==2}"><span class="progress">合格</span></c:if>
			</div>
		</div>
		<div class="contents">
			<button onclick="location.href='/ELearningTeam/question1'" class="button">問題スタート</button>
		</div><br /><br />
		<div class="copyright">Copyright(C)西宮原庄</div>
	</div>
</body>
</html>