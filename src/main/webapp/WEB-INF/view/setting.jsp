<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>編集画面</title>
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet" >
</head>
<body>
	<a href="/ELearningTeam/home"><img src="https://www.alhinc.jp/img/common/h-logo.svg" width="150" height="100"></a>
	<div class="main-contents">
		<div class="setting">

			<div class="errorMessages">
				<ul>
					<c:out value="${ errorMessages }"></c:out>
					<br />
					<c:out value="${ validation }"></c:out>
				</ul>
			</div>

			<form:form modelAttribute="settingForm"
				action="/ELearningTeam/setting/${user.id}">
				<input name="id" value="${checkUser.id}" id="id" type="hidden" />
				<div>
					<form:errors path="*" />
				</div>
				<h2>${checkUser.name }さんの編集画面</h2>
				<h3>${passwordMessage}</h3>
				<br />
				<c:if
					test="${loginUser.divisionId==1 && loginUser.id != checkUser.id}">
					<a href="/ELearningTeam/management/">ユーザー一覧</a>
					<br />
					<br />
					<label for="divisionId">部署名</label>
					<br />
					<input type="radio" name="divisionId" value="1" id="divisionId"
						checked="checked" />WBU<br />
					<input type="radio" name="divisionId" value="2" id="divisionId" />受験者<br />

				</c:if>

				<c:if
					test="${loginUser.divisionId==1 && loginUser.id == checkUser.id}">
					<a href="/ELearningTeam/management/">ユーザー一覧</a><br />
					<label for="password">パスワード</label>
					<input name="password" type="password" class="field" id="password" placeholder="8文字以上のパスワード"/>
					<br />
					<input type="checkbox" id="password-check">パスワードを表示する<br />
					<script>
		 				const pwd = document.getElementById('password');
		 				const pwdCheck = document.getElementById('password-check');
		 				pwdCheck.addEventListener('change', function() {
		     				if(pwdCheck.checked) {
		         				pwd.setAttribute('type', 'text');
		     				} else {
		         				pwd.setAttribute('type', 'password');
		     				}
		 				}, false);
		 			</script>
					<br />
					<label for="confirmPassword">確認用パスワード</label>
					<input name="passwordConfirm" type="password" class="field" id="passwordConfirm" /><br />
					<input type="checkbox" id="password-check">パスワードを表示する<br />
					<script>
		 				const pwd = document.getElementById('password');
		 				const pwdCheck = document.getElementById('password-check');
		 				pwdCheck.addEventListener('change', function() {
		     				if(pwdCheck.checked) {
		         				pwd.setAttribute('type', 'text');
		     				} else {
		         				pwd.setAttribute('type', 'password');
		     				}
		 				}, false);
		 			</script>
					<br />

				</c:if>

				<c:if test="${loginUser.divisionId==2 }">
					<label for="password">パスワード</label>
					<input name="password" type="password" class="field" id="password"   placeholder="8文字以上のパスワード"/>
					<br />
					<input type="checkbox" id="password-check">パスワードを表示する<br /><br/>
					<script>
	 				const pwd = document.getElementById('password');
	 				const pwdCheck = document.getElementById('password-check');
	 				pwdCheck.addEventListener('change', function() {
	     				if(pwdCheck.checked) {
	         				pwd.setAttribute('type', 'text');
	     				} else {
	         				pwd.setAttribute('type', 'password');
	     				}
	 				}, false);
	 				</script>
					<label for="confirmPassword">確認用パスワード</label>
					<input name="passwordConfirm" type="password" class="field" id="passwordConfirm" /><br />
					<input type="checkbox" id="password-check">パスワードを表示する<br />
					<script>
		 				const pwd = document.getElementById('password');
		 				const pwdCheck = document.getElementById('password-check');
		 				pwdCheck.addEventListener('change', function() {
		     				if(pwdCheck.checked) {
		         				pwd.setAttribute('type', 'text');
		     				} else {
		         				pwd.setAttribute('type', 'password');
		     				}
		 				}, false);
		 			</script>
					<br />
					<input type="hidden" name="divisionId" id="divisionId"
						value="${checkUser.divisionId}">
				</c:if>
				<input type ="submit" value = "編集" class="button"><br/>
			</form:form>
			<div class="copyright">Copyright(C)西宮原庄</div>
		</div>
	</div>

	<c:remove var = "passwordMessages" scope = "session"/>
	<c:remove var = "errorMessages" scope = "session"/>
	<c:remove var = "validation" scope = "session"/>
</body>
</html>