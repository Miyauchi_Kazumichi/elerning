<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
	<head>
	    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	    <title>セキュリティ問題</title>
	     <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet" >
	</head>
	<body>
	<a href="/ELearningTeam/home"><img src="https://www.alhinc.jp/img/common/h-logo.svg" width="150" height="100"></a>
		<div class="main-contents">
		    <h1>セキュリティ問題</h1>

		    <form:form modelAttribute="questionForm" action="confirm" method="get" >
		        <div class="question" title="問1"><c:out value="${question1}" /></div><br />
		        <div class="choice"><form:radiobuttons path="choiceText1" delimiter="<br>" items="${choices40}" /></div><br /><br />

		        <div class="question" title="問2"><c:out value="${question2}" /></div><br />
		        <div class="choice"><form:radiobuttons path="choiceText2" delimiter="<br>" items="${choices41}" /></div><br /><br />

		        <div class="question" title="問3"><c:out value="${question3}" /></div><br />
		        <div class="choice"><form:radiobuttons path="choiceText3" delimiter="<br>" items="${choices42}" /></div><br /><br />

		        <div class="question" title="問4"><c:out value="${question4}" /></div><br />
		        <div class="choice"><form:radiobuttons path="choiceText4" delimiter="<br>" items="${choices43}" /></div><br /><br />

		        <div class="question" title="問5"><c:out value="${question5}" /></div><br />
		      	<div class="choice"><form:radiobuttons path="choiceText5" delimiter="<br>" items="${choices44}" /></div><br /><br />

		        <div class="question" title="問6"><c:out value="${question6}" /></div><br />
		        <div class="choice"><form:radiobuttons path="choiceText6" delimiter="<br>" items="${choices45}" /></div><br /><br />

		        <div class="question" title="問7"><c:out value="${question7}" /></div><br />
		        <div class="choice"><form:radiobuttons path="choiceText7" delimiter="<br>" items="${choices46}" /></div><br /><br />

		        <div class="question" title="問8"><c:out value="${question8}" /></div><br />
		        <div class="choice"><form:radiobuttons path="choiceText8" delimiter="<br>" items="${choices47}" /></div><br /><br />

		        <div class="question" title="問9"><c:out value="${question9}" /></div><br />
		        <div class="choice"><form:radiobuttons path="choiceText9" delimiter="<br>" items="${choices48}" /></div><br /><br />

		        <div class="question" title="問10"><c:out value="${question10}" /></div><br />
		       <div class="choice"> <form:radiobuttons path="choiceText10" delimiter="<br>" items="${choices49}" /></div><br /><br />
		        <input type="submit" value="送信" class="button" /><br />
			</form:form>
			<div class="copyright">Copyright(C)西宮原庄</div>
		</div>
	</body>
</html>