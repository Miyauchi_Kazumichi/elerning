<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>ログイン画面</title>
		<link href="<c:url value="/resources/css/style.css" />" rel="stylesheet" >
	</head>
	<body>
		<a href="/ELearningTeam/home"><img src="https://www.alhinc.jp/img/common/h-logo.svg" width="150" height="100"></a>
		<div class="main-contents">
			<h1>${message}</h1>

			<c:remove var = "loginUser" scope = "session"/>

			<form:form modelAttribute="loginForm">
			<div>
				<form:errors path="*" />
			</div>
			アカウント名：<input type="text" id="account" name="account" value = "<c:out value="${ account }"/>"><br/>

			パスワード：<input type="password" class="field" id="password" name="password"><br/>
			<input type="checkbox" id="password-check">パスワードを表示する<br/>
			<input type="submit" value="ログイン" class="button">
			<script>
 				const pwd = document.getElementById('password');
 				const pwdCheck = document.getElementById('password-check');
 				pwdCheck.addEventListener('change', function() {
     				if(pwdCheck.checked) {
         				pwd.setAttribute('type', 'text');
     				} else {
         				pwd.setAttribute('type', 'password');
     				}
 				}, false);
 			</script>
			</form:form><br /><br />
			<div class="copyright">Copyright(C)西宮原庄</div>
		</div>
	</body>
</html>