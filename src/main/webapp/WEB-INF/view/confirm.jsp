<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page isELIgnored="false"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>送信前確認画面</title>
		 <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet" >
	</head>
	<body>
		<a href="/ELearningTeam/home"><img src="https://www.alhinc.jp/img/common/h-logo.svg" width="150" height="100"></a>
		<div class="main-contents">
		<h1>送信前確認画面</h1>
		<form:form modelAttribute="questionForm" action="result" method="get">

			<div class="confirm-question" title="問1">
			<c:out value="${question1}" /><br /><br/>
			</div>
			<div class="selected-answer" title="あなたが選んだ答え">
			<c:out value="${choice1}" /><br />
			</div>

			<div class="confirm-question" title="問2">
			<c:out value="${question2}" /><br /><br/>
			</div>
			<div class="selected-answer" title="あなたが選んだ答え">
			<c:out value="${choice2}" /><br />
			</div>

			<div class="confirm-question" title="問3">
			<c:out value="${question3}" /><br /><br/>
			</div>
			<div class="selected-answer" title="あなたが選んだ答え">
			<c:out value="${choice3}" /><br />
			</div>

			<div class="confirm-question" title="問4">
			<c:out value="${question4}" /><br /><br/>
			</div>
			<div class="selected-answer" title="あなたが選んだ答え">
			<c:out value="${choice4}" /><br />
			</div>

			<div class="confirm-question" title="問5">
			<c:out value="${question5}" /><br /><br/>
			</div>
			<div class="selected-answer" title="あなたが選んだ答え">
			<c:out value="${choice5}" /><br />
			</div>

			<div class="confirm-question" title="問6">
			<c:out value="${question6}" /><br /><br/>
			</div>
			<div class="selected-answer" title="あなたが選んだ答え">
			<c:out value="${choice6}" /><br />
			</div>

			<div class="confirm-question" title="問7">
			<c:out value="${question7}" /><br /><br/>
			</div>
			<div class="selected-answer" title="あなたが選んだ答え">
			<c:out value="${choice7}" /><br />
			</div>

			<div class="confirm-question" title="問8">
			<c:out value="${question8}" /><br /><br/>
			</div>
			<div class="selected-answer" title="あなたが選んだ答え">
			<c:out value="${choice8}" /><br />
			</div>

			<div class="confirm-question" title="問9">
			<c:out value="${question9}" /><br /><br/>
			</div>
			<div class="selected-answer" title="あなたが選んだ答え">
			<c:out value="${choice9}" /><br />
			</div>

			<div class="confirm-question" title="問10">
			<c:out value="${question10}" /><br /><br/>
			</div>
			<div class="selected-answer" title="あなたが選んだ答え">
			<c:out value="${choice10}" /><br />
			</div>

		<input name = "userProgress" id = "userProgress" value = "${userProgress}" type = "hidden"><br />

			<input type = "submit" value = "送信" class="button">
		</form:form>
		<form:form modelAttribute = "resultForm">
			 <a href="javascript:void(0)" onclick="javascript:history.back()" class="button">回答を修正する</a>
		</form:form><br /><br />
		<div class="copyright">Copyright(C)西宮原庄</div>
		</div>
	</body>
</html>