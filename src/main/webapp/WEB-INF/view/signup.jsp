<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page isELIgnored="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー新規登録</title>
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet" >
</head>
<body>
	<a href="/ELearningTeam/home"><img src="https://www.alhinc.jp/img/common/h-logo.svg" width="150" height="100"></a>
	<div class="main-contents">
		<div class="header-signup">
			<h1>新規登録</h1>
			<a href="/ELearningTeam/home/" class="button">ホーム</a>
			<a href="/ELearningTeam/management/" class="button">ユーザー一覧</a>
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${ errorMessages }" var="errorMessage">
							<c:out value="${ errorMessage }"></c:out>
						</c:forEach>
					</ul>
				</div>
			</c:if>
		</div>

		<div class="main-contents-signup">

			<form:form modelAttribute="userForm" action="/ELearningTeam/signup">
				<br />
				<div>
					<form:errors path="*" />
				</div>

				<label for="name">名前</label>
				<br />
				<br />
				<input name="name" id="name" value="<c:out value="${ name }"/>">
				<br />
				<br />

				<label for="account">アカウント</label>
				<br />
				<br />
				<input name="account" id="account"
					value="<c:out value="${ account }"/>">
				<br />
				<br />

				<label for="password">パスワード</label>
				<br />
				<br />

				<input name="password" type="password" class="field" id="password"><br />
				<input type="checkbox" id="password-check">パスワードを表示する<br/>
				<script>
	 				const pwd = document.getElementById('password');
	 				const pwdCheck = document.getElementById('password-check');
	 				pwdCheck.addEventListener('change', function() {
	     				if(pwdCheck.checked) {
	         				pwd.setAttribute('type', 'text');
	     				} else {
	         				pwd.setAttribute('type', 'password');
	     				}
	 				}, false);
	 			</script>
				<br />
				<br />

				<label for="passwordConfirm">確認用パスワード</label>
				<br />
				<br />
				<input type="password" name="passwordConfirm" class="field" id="passwordConfirm"><br />
				<input type="checkbox" id="password-check">パスワードを表示する<br/>
				<script>
	 				const pwd = document.getElementById('password');
	 				const pwdCheck = document.getElementById('password-check');
	 				pwdCheck.addEventListener('change', function() {
	     				if(pwdCheck.checked) {
	         				pwd.setAttribute('type', 'text');
	     				} else {
	         				pwd.setAttribute('type', 'password');
	     				}
	 				}, false);
	 			</script>
				<br />
				<br />

				<label for="divisionId">部署</label>
				<br />
				<br />
				<input type="radio" name="divisionId" value="1" checked="checked">WBU
				<input type="radio" name="divisionId" value="2">受験者<br />

				<input type="submit" value="登録">
				<br />
				<br />

			</form:form>
			<div class="copyright">Copyright(C)西宮原庄</div>
		</div>
	</div>
</body>
</html>
