<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page isELIgnored="false"%>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>結果画面</title>
		 <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet" >
	</head>
	<body>
		<a href="/ELearningTeam/home"><img src="https://www.alhinc.jp/img/common/h-logo.svg" width="150" height="100"></a>
		<div class="main-contents">
			<h1>結果</h1>

		    <div class="message"><c:out value="${message}"></c:out></div>
			<%int count = 0;%>

			<div class="result-question" title="問1">
			<c:out value="${question1}" /><br />
			</div>
			<c:if test = "${choice1 ==  correctAnswer1}">
				<div class="correct"><c:out value="○"></c:out></div><br/>
				<%count += 1;%>
			</c:if>
			<c:if test = "${choice1 !=  correctAnswer1}">
				<div class="not-correct"><c:out value="×"></c:out></div><br/>
			</c:if>
			<div class="choice">
			<c:forEach items = "${choices40}"  var ="choice">
				<c:out value = "${choice}"></c:out><br/>
			</c:forEach><br/>
			</div>
			<div class="explanation" title="解説">
			<c:out value = "${answer1}"></c:out><br/>
			</div>

			<div class="result-question" title="問2">
			<c:out value="${question2}" /><br />
			</div>
			<c:if test = "${choice2 ==  correctAnswer2}">
				<div class="correct"><c:out value="○"></c:out></div><br/>
				<%count += 1;%>
			</c:if>
			<c:if test = "${choice2 !=  correctAnswer2}">
				<div class="not-correct"><c:out value="×"></c:out></div><br/>
			</c:if>
			<div class="choice">
			<c:forEach items = "${choices41}"  var ="choice">
				<c:out value = "${choice}"></c:out><br/>
			</c:forEach><br/>
			</div>
			<div class="explanation" title="解説">
			<c:out value = "${answer2}"></c:out><br/>
			</div>

			<div class="result-question" title="問3">
			<c:out value="${question3}" /><br />
			</div>
			<c:if test = "${choice3 ==  correctAnswer3}">
				<div class="correct"><c:out value="○"></c:out></div><br/>
				<%count += 1;%>
			</c:if>
			<c:if test = "${choice3 !=  correctAnswer3}">
				<div class="not-correct"><c:out value="×"></c:out></div><br/>
			</c:if>
			<div class="choice">
			<c:forEach items = "${choices42}"  var ="choice">
				<c:out value = "${choice}"></c:out><br/>
			</c:forEach><br/>
			</div>
			<div class="explanation" title="解説">
			<c:out value = "${answer3}"></c:out><br/>
			</div>

			<div class="result-question" title="問4">
			<c:out value="${question4}" /><br />
			</div>
			<c:if test = "${choice4 ==  correctAnswer4}">
				<div class="correct"><c:out value="○"></c:out></div><br/>
				<%count += 1;%>
			</c:if>
			<c:if test = "${choice4 !=  correctAnswer4}">
				<div class="not-correct"><c:out value="×"></c:out></div><br/>
			</c:if>
			<div class="choice">
			<c:forEach items = "${choices43}"  var ="choice">
				<c:out value = "${choice}"></c:out><br/>
			</c:forEach><br/>
			</div>
			<div class="explanation" title="解説">
			<c:out value = "${answer4}"></c:out><br/>
			</div>

			<div class="result-question" title="問5">
			<c:out value="${question5}" /><br />
			</div>
			<c:if test = "${choice5 ==  correctAnswer5}">
				<div class="correct"><c:out value="○"></c:out></div><br/>
				<%count += 1;%>
			</c:if>
			<c:if test = "${choice5 !=  correctAnswer5}">
				<div class="not-correct"><c:out value="×"></c:out></div><br/>
			</c:if>
			<div class="choice">
			<c:forEach items = "${choices44}"  var ="choice">
				<c:out value = "${choice}"></c:out><br/>
			</c:forEach><br/>
			</div>
			<div class="explanation" title="解説">
			<c:out value = "${answer5}"></c:out><br/>
			</div>

			<div class="result-question" title="問6">
			<c:out value="${question6}" /><br />
			</div>
			<c:if test = "${choice6 ==  correctAnswer6}">
				<div class="correct"><c:out value="○"></c:out></div><br/>
				<%count += 1;%>
			</c:if>
			<c:if test = "${choice6 !=  correctAnswer6}">
				<div class="not-correct"><c:out value="×"></c:out></div><br/>
			</c:if>
			<div class="choice">
			<c:forEach items = "${choices45}"  var ="choice">
				<c:out value = "${choice}"></c:out><br/>
			</c:forEach><br/>
			</div>
			<div class="explanation" title="解説">
			<c:out value = "${answer6}"></c:out><br/>
			</div>

			<div class="result-question" title="問7">
			<c:out value="${question7}" /><br />
			</div>
			<c:if test = "${choice7 ==  correctAnswer7}">
				<div class="correct"><c:out value="○"></c:out></div><br/>
				<%count += 1;%>
			</c:if>
			<c:if test = "${choice7 !=  correctAnswer7}">
				<div class="not-correct"><c:out value="×"></c:out></div><br/>
			</c:if>
			<div class="choice">
			<c:forEach items = "${choices46}"  var ="choice">
				<c:out value = "${choice}"></c:out><br/>
			</c:forEach><br/>
			</div>
			<div class="explanation" title="解説">
			<c:out value = "${answer7}"></c:out><br/>
			</div>

			<div class="result-question" title="問8">
			<c:out value="${question8}" /><br />
			</div>
			<c:if test = "${choice8 ==  correctAnswer8}">
				<div class="correct"><c:out value="○"></c:out></div><br/>
				<%count += 1;%>
			</c:if>
			<c:if test = "${choice8 !=  correctAnswer8}">
				<div class="not-correct"><c:out value="×"></c:out></div><br/>
			</c:if>
			<div class="choice">
			<c:forEach items = "${choices47}"  var ="choice">
				<c:out value = "${choice}"></c:out><br/>
			</c:forEach><br/>
			</div>
			<div class="explanation" title="解説">
			<c:out value = "${answer8}"></c:out><br/>
			</div>

			<div class="result-question" title="問9">
			<c:out value="${question9}" /><br />
			</div>
			<c:if test = "${choice9 ==  correctAnswer9}">
				<div class="correct"><c:out value="○"></c:out></div><br/>
				<%count += 1;%>
			</c:if>
			<c:if test = "${choice9 !=  correctAnswer9}">
				<div class="not-correct"><c:out value="×"></c:out></div><br/>
			</c:if>
			<div class="choice">
			<c:forEach items = "${choices48}"  var ="choice">
				<c:out value = "${choice}"></c:out><br/>
			</c:forEach><br/>
			</div>
			<div class="explanation" title="解説">
			<c:out value = "${answer9}"></c:out><br/>
			</div>

			<div class="result-question" title="問10">
			<c:out value="${question10}" /><br />
			</div>
			<c:if test = "${choice10 ==  correctAnswer10}">
				<div class="correct"><c:out value="○"></c:out></div><br/>
				<%count += 1;%>
			</c:if>
			<c:if test = "${choice10 !=  correctAnswer10}">
				<div class="not-correct"><c:out value="×"></c:out></div><br/>
			</c:if>
			<div class="choice">
			<c:forEach items = "${choices49}"  var ="choice">
				<c:out value = "${choice}"></c:out><br/>
			</c:forEach><br/>
			</div>
			<div class="explanation" title="解説">
			<c:out value = "${answer10}"></c:out><br/>
			</div>

			<%= count%><c:out value="点"/><br />

			<form:form modelAttribute = "questionForm">
				<input name = "id" id = "id" value = "${loginUser.id}" type = "hidden">
				<input type = "submit" value = "Topへ" class="button">
			</form:form>
			<div class="copyright">Copyright(C)西宮原庄</div>
		</div>
	</body>
</html>