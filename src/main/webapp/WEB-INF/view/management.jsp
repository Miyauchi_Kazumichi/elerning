<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー一覧</title>
        <link href="<c:url value="/resources/css/style.css" />" rel="stylesheet" >
</head>
<body>
	<a href="/ELearningTeam/home"><img src="https://www.alhinc.jp/img/common/h-logo.svg" width="150" height="100"></a>
	<div class="main-contents">
		<a href="/ELearningTeam/home" class="button">ホーム</a>
		<a href="/ELearningTeam/signup" class="button">新規登録</a>
		<h1>ユーザー一覧</h1>
		<div class="container">
			<c:forEach items="${users}" var="user">
				<div class="userlist">
					<form:form modelAttribute="settingForm">
						<input type="hidden" name="id" value="${user.id}" id="id" />
						アカウント名: <c:out value="${user.account}" /><br />
						氏名: <c:out value="${user.name }" /><br />
						所属: <c:if test="${user.divisionId==1 }">WBU</c:if>
						<c:if test="${user.divisionId==2 }">受験者</c:if><br />
						受験状況: <c:if test="${user.progress==0}">未受験</c:if>
						<c:if test="${user.progress==1}">不合格</c:if>
						<c:if test="${user.progress==2}">合格</c:if><br />
						作成日: <c:out value="${user.createdDate }" /><br />
						最終更新日: <c:out value="${user.updatedDate }" /><br />
						<a href="/ELearningTeam/setting/${user.id}" class="button">編集</a>
					</form:form>
				</div>
			</c:forEach>
			<br /><br />
			<div class="copyright">Copyright(C)西宮原庄</div>
		</div>
	</div>
</body>
</html>